import React from "react";
import { useSelector } from "react-redux";
import { ClockLoader } from "react-spinners";

export default function Spinner() {
  let { isLoading } = useSelector((state) => state.spinnerSlice);
  return isLoading ? (
    <div className="fixed top-0 left-0 z-50 bg-black h-screen w-screen flex flex-col justify-center items-center">
      <ClockLoader color="gold" size={200} />
      <br />
      <h2 className="text-white text-4xl">LOADING</h2>
    </div>
  ) : null;
}
