import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";
import logo from "../../assets/logo.webp";
import { Desktop, Tablet } from "../../HOC/Responsive";
import { useSelector } from "react-redux";

export default function Header() {
  let user = useSelector((state) => state.userSlice.userInfo);
  let adminLink = <NavLink to="/admin/user">Admin Page</NavLink>;

  return (
    <div className="bg-black flex px-10 py-5 shadow justify-between items-center">
      <NavLink to="/">
        {/* <span className="text-red-600 font-medium text-xl">CyberFlix</span> */}
        <img style={{ height: "50px" }} src={logo} alt="Logo" />
      </NavLink>
      <div className="text-white">
        <Desktop>
          {user !== null && user.maLoaiNguoiDung == "QuanTri"
            ? adminLink
            : null}
        </Desktop>
        <Tablet>
          {user !== null && user.maLoaiNguoiDung == "QuanTri"
            ? adminLink
            : null}
        </Tablet>
        {/* {" | "} */}
        {/* <NavLink to="/signup">Sign Up</NavLink>  */}
        {/* <NavLink to="/">Danh Sách Rạp</NavLink> */}
      </div>
      <UserNav />
    </div>
  );
}
