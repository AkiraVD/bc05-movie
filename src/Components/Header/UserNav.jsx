import { MenuOutlined } from "@ant-design/icons";
import { Button, Drawer } from "antd";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";
import { userLocalService } from "../../services/localStorageService";

export default function UserNav() {
  let user = useSelector((state) => state.userSlice.userInfo);
  const adminLink = () => {
    return <NavLink to="/admin/user">Admin Page</NavLink>;
  };

  const [open, setOpen] = useState(false);

  let handleLogout = () => {
    userLocalService.remove();
    window.location.href = "/";
  };

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };
  const renderContent = () => {
    if (user) {
      return (
        <>
          <NavLink to="/user">
            <span className="text-yellow-500">{user.hoTen}</span>
          </NavLink>
          <Button
            type="button"
            className="sm:ml-10 bg-yellow-500 hover:bg-black hover:text-yellow-500 hover:border-yellow-500"
            onClick={handleLogout}
          >
            Đăng xuất
          </Button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            <Button
              type="button"
              className=" bg-yellow-500 hover:bg-black hover:text-yellow-500 hover:border-yellow-500"
            >
              Đăng nhập
            </Button>
          </NavLink>
          <NavLink to="/signup">
            <Button
              type="button"
              className=" bg-yellow-500 hover:bg-black hover:text-yellow-500 hover:border-yellow-500"
            >
              Đăng ký
            </Button>
          </NavLink>
        </>
      );
    }
  };
  return (
    <>
      <Desktop>
        <div className="space-x-5 flex">{renderContent()}</div>
      </Desktop>
      <Tablet>
        <div className="space-x-5 flex">{renderContent()} </div>
      </Tablet>
      <Mobile>
        <MenuOutlined
          className="text-yellow-500 text-4xl border-2 border-yellow-500 px-2 pb-2 rounded hover:bg-yellow-500 hover:text-black duration-300 inline-block"
          onClick={showDrawer}
        />
        <Drawer
          headerStyle={{ display: "none" }}
          width="70%"
          rootClassName="opacity-90"
          bodyStyle={{ background: "rgb(0,0,0)" }}
          title=""
          placement="right"
          onClose={onClose}
          open={open}
        >
          <div className="flex flex-col justify-center items-center gap-2 my-5">
            {renderContent()}
          </div>
          <div className="w-full border-t border-yellow-500 text-center p-5 text-white">
            {user !== null && user?.maLoaiNguoiDung == "QuanTri"
              ? adminLink()
              : null}
          </div>
        </Drawer>
      </Mobile>
    </>
  );
}
