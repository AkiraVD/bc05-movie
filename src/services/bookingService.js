import { https } from "./configURL";

export const bookingService = {
  getDanhSachPhongVe: (maLichChieu) => {
    return https.get(
      `/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`
    );
  },
  postDatVe: (thongTinDatVe) => {
    return https.post(`/api/QuanLyDatVe/DatVe`, thongTinDatVe);
  },
};
