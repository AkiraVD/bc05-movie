import { https } from "./configURL";

export const movieService = {
  getDanhSachPhim: () => {
    return https.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07`);
  },
  getPhimTheoHeThongRap: () => {
    return https.get(
      `/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP07`
    );
  },
  getBanner: () => {
    return https.get(`/api/QuanLyPhim/LayDanhSachBanner`);
  },
  getMovieDetails: (id) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`);
  },
  getThongTinLichChieu: (maPhim) => {
    return https.get(
      `/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`
    );
  },
};
