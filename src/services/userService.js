import axios from "axios";
import { BASE_URL, createConfig, https } from "./configURL";

export const userService = {
  postDangNhap: (dataUser) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: dataUser,
      headers: createConfig(),
    });
  },
  postTaoTaiKhoan: (dataUser) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
      method: "POST",
      data: dataUser,
      headers: createConfig(),
    });
  },
  postThongTinTaiKhoan: () => {
    return https.post("/api/QuanLyNguoiDung/ThongTinTaiKhoan");
  },
};
