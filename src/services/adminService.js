import { https } from "./configURL";

export const adminService = {
  layDanhSachUser: () => {
    return https.get("/api/QuanLyNguoiDung/LayDanhSachNguoiDung");
  },
  xoaUser: (idUser) => {
    return https.delete(`/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${idUser}`);
  },
};

// axios instance
