export const USER_LOGIN = "USER_LOGIN";

export const userLocalService = {
  set: (userData) => {
    let userJSON = JSON.stringify(userData);
    localStorage.setItem(USER_LOGIN, userJSON);
  },
  get: () => {
    let userJSON = localStorage.getItem(USER_LOGIN);
    // console.log("userJSON: ", userJSON);
    if (userJSON !== null) {
      return JSON.parse(userJSON);
    } else {
      return null;
    }
  },
  remove: () => {
    localStorage.removeItem(USER_LOGIN);
  },
};
