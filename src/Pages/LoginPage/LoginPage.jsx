import { Button, Form, Input, message } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { userLocalService } from "../../services/localStorageService";
import { userService } from "../../services/userService";
import Lottie from "lottie-react";
import bg_animate from "../../assets/90315-christmas-tree.json";
import { setUserInfo } from "../../redux-toolkit/slice/userSlice";

export default function LoginPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const onFinish = (values) => {
    console.log("Success:", values);
    userService
      .postDangNhap(values)
      .then((res) => {
        dispatch(setUserInfo(res.data.content));
        console.log(res);
        message.success("Đăng nhập thành công");
        // Luu local storage
        userLocalService.set(res.data.content);
        setTimeout(() => {
          // window.location.href = "/";
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error(err.response.data.content);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const glassStyle = {
    boxShadow: "0 8px 32px 0 rgba( 31, 38, 135, 0.37 )",
    backdropFilter: "blur( 2px )",
    borderRadius: "10px",
    border: "1px solid rgba( 255, 255, 255, 0.18 )",
  };

  return (
    <div className="w-screen h-screen flex justify-center items-center">
      <div className="container p-5 flex h-full flex-col md:flex-row justify-center items-center">
        <div className="w-full h-1/2 md:h-full md:max-w-[40vw] md:w-1/2 flex justify-center items-center">
          <Lottie animationData={bg_animate} loop={true} />
        </div>
        <div
          style={glassStyle}
          className="w-full md:w-1/2 h-min self-center p-5 bg-black/50 md:ml-10"
        >
          <h2 className="text-4xl mb-10 text-center text-yellow-500 font-bold">
            Đăng nhập
          </h2>
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label={<label className="text-yellow-500">Tài khoản</label>}
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Xin vui lòng nhập tài khoản!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label={<label className="text-yellow-500">Mật khẩu</label>}
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Xin vui lòng nhập mật khẩu!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <div className="flex flex-col md:flex-row justify-center items-center md:items-start">
              <Form.Item>
                <Button
                  className="bg-blue-800 text-white hover:bg-transparent hover:text-blue-800"
                  htmlType="submit"
                >
                  Login
                </Button>
              </Form.Item>
              <NavLink to="/">
                <Button
                  type="button"
                  className="md:ml-10 bg-yellow-500 hover:bg-transparent hover:text-yellow-500 hover:border-yellow-500"
                >
                  Quay về trang chủ &#x2192;
                </Button>
              </NavLink>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}
