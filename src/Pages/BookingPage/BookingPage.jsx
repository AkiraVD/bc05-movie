import { message } from "antd";
import { nanoid } from "nanoid";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { bookingService } from "../../services/bookingService";

export default function BookingPage() {
  let params = useParams();
  let user = useSelector((state) => state.userSlice.userInfo);
  let navigate = useNavigate();

  const [bookingDetails, setBookingDetails] = useState({});
  const [seats, setSeats] = useState([]);
  const [selectedSeat, setSelectedSeat] = useState([]);
  const [total, setTotal] = useState([]);

  useEffect(() => {
    bookingService
      .getDanhSachPhongVe(params.id)
      .then((res) => {
        setBookingDetails(res.data.content.thongTinPhim);
        setSeats(
          res.data.content.danhSachGhe.map((item) => {
            return { ...item, isSelected: false };
          })
        );
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  useEffect(() => {
    if (user == null) {
      message.error("Bạn chưa thể đặt vé trước khi đăng nhập");
      navigate("/login");
    }
  }, [user]);

  const handleSelectSeat = (index) => {
    let newSeats = [...seats];
    newSeats[index].isSelected = !newSeats[index].isSelected;
    setSeats(newSeats);

    let newSelected = newSeats.filter((item) => {
      return item.isSelected == true;
    });
    setSelectedSeat(newSelected);

    let sum = 0;
    newSelected.forEach((item) => {
      sum += item.giaVe;
    });
    setTotal(sum);
  };

  const handleRenderDanhSachGheNgoi = () => {
    return seats.map((item, index) => {
      if (item.daDat) {
        return (
          <div
            key={nanoid()}
            data={item.maGhe}
            type="checkbox"
            aria-checked={true}
            className="appearance-none bg-gray-500 flex justify-center items-center aspect-square rounded"
          >
            {item.stt}
          </div>
        );
      } else if (item.loaiGhe == "Vip") {
        return (
          <div
            key={nanoid()}
            type="checkbox"
            aria-checked={false}
            className={`appearance-none  flex justify-center items-center aspect-square rounded cursor-pointer duration-300 border-4 border-yellow-500 hover:border-green-600 ${
              item.isSelected
                ? "bg-green-600 border-green-600"
                : "bg-yellow-500"
            }`}
            onClick={() => {
              handleSelectSeat(index);
            }}
          >
            {item.stt}
          </div>
        );
      }
      return (
        <div
          key={nanoid()}
          data={item.maGhe}
          type="checkbox"
          aria-checked={false}
          className={`appearance-none  flex justify-center items-center aspect-square rounded cursor-pointer duration-300 border-4 hover:border-green-600 ${
            item.isSelected ? "bg-green-600 border-green-600" : "bg-gray-200"
          }`}
          onClick={() => {
            handleSelectSeat(index);
          }}
        >
          {item.stt}
        </div>
      );
    });
  };

  const handleSummit = () => {
    if (total == 0) {
      return message.error("Bạn chưa chọn ghế");
    }
    let thongTinDatVe = {
      maLichChieu: params.id,
      danhSachVe: selectedSeat.map((item) => {
        return {
          maGhe: item.maGhe,
          giaVe: item.giaVe,
        };
      }),
    };
    console.log(thongTinDatVe);
    bookingService
      .postDatVe(thongTinDatVe)
      .then((res) => {
        message.success("Bạn đã đặt vé thành công");
        setTimeout(() => {
          window.location.reload(false);
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error("Lỗi");
      });
    // setTimeout(() => {
    //   navigate("/");
    // }, 1000);
  };

  const formatter = new Intl.NumberFormat("vn-VN", {
    style: "currency",
    currency: "VND",
  });

  return (
    <div className="container mx-auto bg-black/50">
      <h2 className="font-bold text-yellow-500 text-2xl text-center mt-2">
        Mời bạn chọn ghế
      </h2>
      <div className="flex flex-col lg:flex-row p-4 justify-between rounded">
        <div className=" w-full mb-4 rounded">
          <div className="grid grid-cols-6 sm:grid-cols-10 lg:grid-cols-16 gap-2">
            {handleRenderDanhSachGheNgoi()}
          </div>
          <div className="lg:w-1/2 flex gap-4 mt-5">
            <div className="bg-gray-500 w-1/3 rounded text-center">Đã đặt</div>
            <div className="bg-yellow-500 w-1/3 rounded text-center">VIP</div>
            <div className="bg-gray-200 w-1/3 rounded text-center">Thường</div>
          </div>
        </div>
        <div className="w-full lg:w-[500px] bg-black/50 lg:ml-10 mx-auto rounded flex flex-col justify-between">
          <div className="font-semibold text-white">
            <div className="border-b border-yellow-500 py-3 mx-3">
              <div className="w-[100px] inline-block">Tên phim :</div>
              <span className="text-yellow-500">{bookingDetails.tenPhim}</span>
            </div>
            <div className="border-b border-yellow-500 py-3 mx-3">
              <div className="w-[100px] inline-block">Cụm rạp :</div>
              <span>{bookingDetails.tenCumRap}</span>
            </div>
            <div className="border-b border-yellow-500 py-3 mx-3">
              <div className="w-[100px] inline-block">Địa chỉ :</div>
              <span>{bookingDetails.diaChi}</span>
            </div>
            <div className="border-b border-yellow-500 py-3 mx-3">
              <div className="w-[100px] inline-block">Thời gian :</div>
              <span className="text-green-500">{bookingDetails.ngayChieu}</span>
              {" ~ "}
              <span className="text-red-500">{bookingDetails.gioChieu}</span>
            </div>
            <div className="border-b border-yellow-500 py-3 mx-3">
              <div className="w-[100px] inline-block">Rạp :</div>
              <span>{bookingDetails.tenRap}</span>
            </div>

            <div className="border-b border-yellow-500 py-3 mx-3">
              <div className="w-[100px] inline-block">Ghế :</div>
              <span>
                {selectedSeat.map((item) => {
                  return (
                    <span
                      className={`${
                        item.loaiGhe == "Vip" ? "text-yellow-500" : null
                      }`}
                    >
                      {item.loaiGhe == "Vip" ? "V" : "T"}
                      {item.tenGhe},{" "}
                    </span>
                  );
                })}
              </span>
            </div>
            <div className="p-3 font-bold">
              <h3>Thành tiền :</h3>
              <h3 className="text-center text-2xl text-red-600">
                {formatter.format(total)}
              </h3>
            </div>
          </div>
          <button
            className="bg-yellow-500 w-full p-2 font-bold bottom-0"
            onClick={handleSummit}
          >
            Đặt vé
          </button>
        </div>
      </div>
    </div>
  );
}
