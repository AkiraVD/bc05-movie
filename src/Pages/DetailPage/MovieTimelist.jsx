import React from "react";
import { Tabs } from "antd";
import moment from "moment";
import { useNavigate } from "react-router-dom";
import { nanoid } from "nanoid";

export default function MovieTimelist({ heThongRap }) {
  const navigate = useNavigate();

  const handleRenderLichChieu = () => {
    return heThongRap.map((item) => {
      return {
        label: (
          <img className="w-16 h-16 object-cover" src={item.logo} alt="" />
        ),
        key: nanoid(),
        children: (
          <Tabs
            tabPosition="top"
            centered="true"
            items={item.cumRapChieu.map((sub) => {
              return {
                label: <h2 className="text-yellow-500">{sub.tenCumRap}</h2>,
                key: nanoid(),
                children: (
                  <div className="bg-black/50 grid grid-cols-3 md:grid-cols-4 lg:grid-cols-6 p-3 rounded gap-3 justify-items-center">
                    {sub.lichChieuPhim?.map((thongtin) => {
                      return (
                        <p
                          key={nanoid()}
                          className="bg-gray-500/25 border-inherit p-2 rounded font-semibold cursor-pointer"
                          onClick={() => {
                            navigate("/booking/" + thongtin.maLichChieu);
                          }}
                        >
                          <span className="text-green-500">
                            {moment(thongtin.ngayChieuGioChieu).format(
                              "DD/MM/YYYY "
                            )}
                          </span>
                          ~
                          <span className="text-red-500">
                            {moment(thongtin.ngayChieuGioChieu).format(
                              " hh:mm"
                            )}
                          </span>
                        </p>
                      );
                    })}
                  </div>
                ),
              };
            })}
          />
        ),
      };
    });
  };
  return (
    <div className="container p-5 h-[90vh]">
      <h2 className="text-yellow-500 text-4xl font-bold text-center">
        CHỌN RẠP VÀ THỜI GIAN ĐỂ ĐẶT VÉ
      </h2>
      <Tabs tabPosition="top" centered="true" items={handleRenderLichChieu()} />
    </div>
  );
}
