import { message, Progress } from "antd";
import moment from "moment";
import { nanoid } from "nanoid";
import React, { useEffect, useState, useRef } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import { movieService } from "../../services/movieService";
import MovieTimelist from "./MovieTimelist";

export default function DetailPage() {
  let params = useParams();
  let navigate = useNavigate();

  const [movieDetails, setMovieDetail] = useState([]);
  const [heThongRap, setHeThongRap] = useState([]);

  const lichChieuRef = useRef(null);
  const infoRef = useRef(null);

  useEffect(() => {
    movieService
      .getThongTinLichChieu(params.id)
      .then((res) => {
        setMovieDetail(res.data.content);
        setHeThongRap(res.data.content.heThongRapChieu);
        infoRef.current.scrollIntoView({ behavior: "smooth" });
      })
      .catch((err) => {
        message.error(err.response.data.content);
        setTimeout(() => {
          navigate("/");
        }, 1000);
      });
  }, []);

  const handleClick = () => {
    lichChieuRef.current.scrollIntoView({ behavior: "smooth" });
  };

  let tagClassName = "text-yellow-500";
  let backgroundStyle = {
    background: "rgba(0, 0, 0, 0.2)",
    borderRadius: "16px",
    boxShadow: "0 4px 30px rgba(0, 0, 0, 0.1)",
    backdropFilter: "blur(5px)",
    border: "1px solid rgba(0, 0, 0, 0.3)",
  };

  return (
    <div
      ref={infoRef}
      style={backgroundStyle}
      className="container mx-auto text-white "
    >
      <div className="flex flex-col sm:flex-row text-left gap-5 p-5">
        <div className="relative w-full sm:w-1/2 lg:w-1/4 rounded overflow-hidden">
          <img
            className="object-cover mx-auto w-full aspect-[2/3]"
            src={movieDetails.hinhAnh}
            alt=""
          />
          {!!movieDetails.hot ? (
            <div className="absolute top-0 right-0 left-0 text-center font-bold bg-red-500 p-2">
              <h2>PHIM ĐANG HOT</h2>
            </div>
          ) : null}
        </div>
        <div className="flex flex-col w-full sm:w-1/2 lg:w-3/4 py-5">
          <div>
            <h2 className="text-2xl font-semibold text-yellow-500 mb-5">
              {movieDetails.tenPhim}
            </h2>
            <h3>
              <span className={tagClassName}>Mô tả : </span>
              {movieDetails.moTa}
            </h3>
            <br />
          </div>
          <div className="flex">
            <div className="flex flex-col gap-3 w-1/2">
              <p>
                <span className={tagClassName}>Phân loại : </span>
                <span className="text-red-500 font-bold">
                  C18 - PHIM DÀNH CHO NGƯỜI TỪ 18 TUỐI TRỞ LÊN
                </span>
              </p>
              <p>
                <span className={tagClassName}>Ngày khởi chiếu: </span>
                <span>
                  {moment(movieDetails.ngayKhoiChieu).format("DD/MM/YYYY")}
                </span>
              </p>
              <NavLink
                className="border-2 bg-yellow-500 rounded p-2 text-center text-black border-yellow-500 hover:bg-black/50 hover:text-yellow-500 hover:border-yellow-500 font-semibold duration-300"
                onClick={handleClick}
              >
                Đặt vé
              </NavLink>
            </div>
            <div className="w-1/2 flex justify-center items-center">
              <Progress
                type="circle"
                percent={movieDetails.danhGia * 10}
                trailColor="gray"
                status="success"
                format={(number) => {
                  return `${number / 10} điểm`;
                }}
              />
            </div>
          </div>
        </div>
      </div>
      <iframe
        title="trailer"
        className="w-full aspect-video"
        src={movieDetails?.trailer}
      ></iframe>
      <div ref={lichChieuRef}>
        <MovieTimelist heThongRap={heThongRap} />
      </div>
    </div>
  );
}
