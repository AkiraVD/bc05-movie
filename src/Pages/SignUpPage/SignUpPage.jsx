import React, { useState, useEffect } from "react";
import { Form, Input, Button, message } from "antd";
import { userService } from "../../services/userService";
import { NavLink, useNavigate } from "react-router-dom";

export default function SignUpPage() {
  const [form] = Form.useForm();
  const [formData, setFormData] = useState({});
  let navigate = useNavigate();

  useEffect(() => {}, []);

  const [showPassword, setShowPassword] = useState(false);

  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    form.validateFields((error, values) => {
      if (!error) {
        setFormData(values);
      } else {
        console.log(formData);
      }
    });
  };

  const handleFinish = (values) => {
    userService
      .postTaoTaiKhoan(values)
      .then((res) => {
        console.log(res);
        message.success("Tạo tài khoản thành công");
        setTimeout(() => {
          message.success("Quay về trang chủ");
        }, 500);
        setTimeout(() => {
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  const glassStyle = {
    boxShadow: "0 8px 32px 0 rgba( 31, 38, 135, 0.37 )",
    backdropFilter: "blur( 2px )",
    borderRadius: "10px",
    border: "1px solid rgba( 255, 255, 255, 0.18 )",
  };

  return (
    <div className="w-screen h-screen flex flex-col sm:flex-row justify-center items-center">
      <div
        style={glassStyle}
        className="h-min p-5 sm:w-1/2 max-w-lg rounded bg-black/50"
      >
        <h2 className="text-4xl text-center text-yellow-500 font-bold">
          Đăng ký
        </h2>
        <br />
        <Form form={form} onSubmit={handleSubmit} onFinish={handleFinish}>
          <Form.Item
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tên tài khoản của bạn!",
              },
              {
                pattern: /^\S*$/,
                message: "Tên tài khoản không được chứa khoảng trắng!",
              },
            ]}
          >
            <Input placeholder="Tên tài khoản" />
          </Form.Item>
          <Form.Item
            name="matKhau"
            rules={[
              { required: true, message: "Vui lòng nhập mật khẩu của bạn!" },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  const errors = [];

                  if (!value || getFieldValue("matKhau").length < 8) {
                    errors.push("Mật khẩu phải có ít nhất 8 ký tự!");
                  }

                  if (!/[A-Z]/.test(value) || !/[a-z]/.test(value)) {
                    errors.push(
                      "Mật khẩu phải chứa ít nhất một chữ cái viết hoa và một chữ cái viết thường!"
                    );
                  }

                  if (!/\d/.test(value)) {
                    errors.push("Mật khẩu phải chứa ít nhất một chữ số!");
                  }

                  if (errors.length > 0) {
                    return Promise.reject(errors);
                  }

                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Input.Password
              placeholder="Mật khẩu"
              eye
              visibilityToggle
              onClick={handleShowPassword}
              visible={showPassword}
            />
          </Form.Item>
          <Form.Item
            name="matKhauXacNhan"
            dependencies={["matKhau"]}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập lại mật khẩu của bạn!",
              },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (!value || getFieldValue("matKhau") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject("Mật khẩu không khớp!");
                },
              }),
            ]}
            className="mb-4"
          >
            <Input.Password
              placeholder="Nhập lại mật khẩu"
              eye
              visibilityToggle
              onClick={handleShowPassword}
              visible={showPassword}
            />
          </Form.Item>
          <Form.Item
            name="hoTen"
            rules={[
              { required: true, message: "Vui lòng nhập họ tên của bạn!" },
            ]}
          >
            <Input placeholder="Họ tên" />
          </Form.Item>
          {/* <Form.Item
            name="maNhom"
            rules={[{ required: true, message: "Vui lòng chọn mã nhóm!" }]}
            className="mb-4"
          >
            <Select placeholder="Chọn mã nhóm">
              <Select.Option value="GP00">GP00</Select.Option>
              <Select.Option value="GP01">GP01</Select.Option>
              <Select.Option value="GP02">GP02</Select.Option>
              <Select.Option value="GP03">GP03</Select.Option>
              <Select.Option value="GP04">GP04</Select.Option>
              <Select.Option value="GP05">GP05</Select.Option>
            </Select>
          </Form.Item> */}

          <Form.Item
            name="email"
            rules={[
              { required: true, message: "Vui lòng nhập email của bạn!" },
              {
                pattern: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
                message: "Vui lòng nhập địa chỉ email hợp lệ!",
              },
            ]}
          >
            <Input type="email" placeholder="Email" />
          </Form.Item>

          <Form.Item
            name="soDt"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập số điện thoại của bạn!",
              },
              {
                pattern: /^(0[3578]|09)[0-9]{8}$/,
                message: "Vui lòng nhập số điện thoại hợp lệ!",
              },
            ]}
          >
            <Input placeholder="Số điện thoại" />
          </Form.Item>

          {/* <Form.Item
            name="maLoaiNguoiDung"
            rules={[
              { required: true, message: "Vui lòng chọn loại tài khoản!" },
            ]}
          >
            <Radio.Group>
              <Radio value="QuanTri" className="text-yellow-500">
                Quản trị
              </Radio>
              <Radio value="KhachHang" className="text-yellow-500">
                Khách hàng
              </Radio>
            </Radio.Group>
          </Form.Item> */}
          <Form.Item>
            <Button
              className="bg-blue-800 text-white hover:text-blue-500 hover:bg-transparent hover:border-blue-800"
              type="button"
              htmlType="submit"
            >
              Đăng ký
            </Button>
            <NavLink to="/">
              <Button
                type="button"
                className="ml-5 px-5 bg-yellow-500 hover:bg-transparent hover:text-yellow-500 hover:border-yellow-500"
              >
                Quay về trang chủ &#x2192;
              </Button>
            </NavLink>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
