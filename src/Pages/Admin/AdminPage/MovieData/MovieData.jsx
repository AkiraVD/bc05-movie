import { Table } from "antd";
import React, { useEffect, useState } from "react";
import { movieService } from "../../../../services/movieService";
import { columnMovie } from "../utilis";

export default function MovieData({ user }) {
  const [movieArr, setMovieArr] = useState([]);

  useEffect(() => {
    movieService
      .getDanhSachPhim()
      .then((res) => {
        console.log(res.data.content);
        let movieList = res.data.content.map((item) => {
          if (user == null || user?.maLoaiNguoiDung !== "QuanTri") {
            return { ...item };
          } else
            return {
              ...item,
              action: (
                <>
                  <button className="px-2 py-1 rounded bg-blue-500 text-white ml-2">
                    Sửa
                  </button>
                  <button
                    className="px-2 py-1 rounded bg-red-500 text-white ml-2"
                    onClick={() => {}}
                  >
                    Xóa
                  </button>
                </>
              ),
            };
        });
        setMovieArr(movieList);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return <Table columns={columnMovie} dataSource={movieArr} />;
}
