import { Button, Modal, Tag } from "antd";
import moment from "moment";

export const columnUser = [
  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
    className: "font-bold",
  },
  {
    title: "Mật khẩu",
    dataIndex: "matKhau",
    key: "matKhau",
  },
  {
    title: "Họ tên",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Mã loại người dùng",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",
    className: "text-center",
    render: (text) => {
      if (text === "QuanTri") {
        return <Tag color="red">Quản trị</Tag>;
      } else if (text === "KhachHang") {
        return <Tag color="blue">Khách hàng</Tag>;
      }
      return <Tag color="gray">{text}</Tag>;
    },
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
    render: (text) => <a href={`mailto:${text}`}>{text}</a>,
  },
  {
    title: "Số ĐT",
    dataIndex: "soDT",
    key: "soDT",
  },
  {
    title: "Thao tác",
    dataIndex: "action",
    key: "action",
  },
];

export const columnMovie = [
  {
    title: "Mã phim",
    dataIndex: "maPhim",
    key: "maPhim",
  },
  {
    title: "Hình ảnh",
    dataIndex: "hinhAnh",
    key: "hinhAnh",
    render: (src) => {
      return (
        <div className="h-[160px] w-[120px]">
          <img src={src} className="h-full w-full object-contain" />
        </div>
      );
    },
  },
  {
    title: "Tên phim",
    dataIndex: "tenPhim",
    key: "tenPhim",
    className: "font-bold",
  },
  {
    title: "Ngày khởi chiếu",
    dataIndex: "ngayKhoiChieu",
    key: "ngayKhoiChieu",
    render: (text) => {
      return <p>{moment(text).format("DD/MM/YYYY - hh:mm")}</p>;
    },
  },
  {
    title: "Đánh giá",
    dataIndex: "danhGia",
    key: "danhGia",
  },
  {
    title: "Trailer",
    dataIndex: "trailer",
    key: "trailer",
  },
  {
    title: "Mô tả",
    dataIndex: "moTa",
    key: "moTa",
  },
  {
    title: "Thao tác",
    dataIndex: "action",
    key: "action",
  },
];
