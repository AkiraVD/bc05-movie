import React, { useEffect, useState } from "react";
import { adminService } from "../../../../services/adminService";
import { message, Table } from "antd";
import { columnUser } from "../utilis";

export default function UserData({ user }) {
  const [userArr, setUserArr] = useState([]);

  useEffect(() => {
    let handleDeleteUser = (id) => {
      adminService
        .xoaUser(id)
        .then((res) => {
          console.log(res);
          message.success("Đã xóa thành công");
          fetchUserList();
        })
        .catch((err) => {
          console.log(err.response.data.content);
          message.error(err.response.data.content);
        });
    };
    let fetchUserList = () => {
      adminService
        .layDanhSachUser()
        .then((res) => {
          let userList = res.data.content.map((item) => {
            if (user == null || user?.maLoaiNguoiDung !== "QuanTri") {
              return { ...item };
            } else
              return {
                ...item,
                action: (
                  <>
                    <button className="px-2 py-1 rounded bg-blue-500 text-white ml-2">
                      Sửa
                    </button>
                    <button
                      className="px-2 py-1 rounded bg-red-500 text-white ml-2"
                      onClick={() => {
                        handleDeleteUser(item.taiKhoan);
                      }}
                    >
                      Xóa
                    </button>
                  </>
                ),
              };
          });
          console.log(userList);
          setUserArr(userList);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    fetchUserList();
  }, []);
  return <Table columns={columnUser} dataSource={userArr} />;
}
