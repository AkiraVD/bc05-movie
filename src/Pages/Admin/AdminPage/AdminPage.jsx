import React, { useEffect, useState } from "react";

import Header from "../../../Components/Header/Header";
import { useSelector } from "react-redux";
import UserData from "./UserData/UserData";
import { Tabs } from "antd";
import MovieData from "./MovieData/MovieData";

export default function AdminPage() {
  let user = useSelector((state) => state.userSlice.userInfo);

  const items = [
    {
      key: "1",
      label: `Danh sách người dùng`,
      children: <UserData user={user} />,
    },
    {
      key: "2",
      label: `Danh sách phim`,
      children: <MovieData user={user} />,
    },
  ];
  const onChange = (key) => {
    console.log(key);
  };
  return (
    <>
      <Header />
      <div className="container w-full mx-auto">
        <Tabs defaultActiveKey="1" items={items} onChange={onChange} centered />
      </div>
    </>
  );
}
