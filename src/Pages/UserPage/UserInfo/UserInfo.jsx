import { Tag } from "antd";
import React from "react";

export default function UserInfo({ user }) {
  return (
    <>
      <h2 className="text-4xl font-bold text-yellow-500 mb-5">
        Thông tin tài khoản
      </h2>
      <div className="sm:ml-20 text-white  leading-loose ">
        <div>
          <div className="w-[120px] inline-block">Tên tài khoản</div>
          {" : "}
          <span className="ml-3 font-bold">{user?.taiKhoan}</span>
        </div>
        <div>
          <div className="w-[120px] inline-block">Họ tên</div>
          {" : "}
          <span className="ml-3 font-bold">{user?.hoTen}</span>
        </div>
        <div>
          <div className="w-[120px] inline-block">Email</div>
          {" : "}
          <span className="ml-3 font-bold">{user?.email}</span>
        </div>
        <div>
          <div className="w-[120px] inline-block">Số điện thoại</div>
          {" : "}
          <span className="ml-3 font-bold">{user?.soDT}</span>
        </div>
        <div>
          <div className="w-[120px] inline-block">Loại người dùng</div>
          {" : "}
          <span className="ml-3 font-bold">
            {user?.maLoaiNguoiDung == "QuanTri" ? (
              <Tag color="red">Quản trị</Tag>
            ) : (
              <Tag color="blue">Khách hàng</Tag>
            )}
          </span>
        </div>
      </div>
    </>
  );
}
