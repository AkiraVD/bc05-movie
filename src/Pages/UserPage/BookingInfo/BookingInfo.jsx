import moment from "moment";
import React from "react";

export default function BookingInfo({ thongTinDatVe }) {
  const handleRenderDanhSachPhimDaDat = () => {
    return thongTinDatVe?.map((item) => {
      return (
        <div className="flex p-4 border border-gray-500 rounded gap-4">
          <div className="w-1/3">
            <img src={item.hinhAnh} className="" />
          </div>
          <div className="font-semibold">
            <h2 className="text-2xl text-yellow-500 mb-5 capitalize">
              {item.tenPhim}
            </h2>
            <p>
              <span className="text-yellow-500">Thời lượng : </span>
              {item.thoiLuongPhim} + phút
            </p>
            <p>
              <span className="text-yellow-500">Ngày đặt : </span>
              {moment(item.ngayDat).format("DD/MM/YYYY - hh:mm")}
            </p>
            <p>
              <span className="text-yellow-500">Tên rạp : </span>
              {item.danhSachGhe[0].tenHeThongRap} - {item.danhSachGhe[0].tenRap}
            </p>
            <p>
              <span className="text-yellow-500">Ghế : </span>
              {item.danhSachGhe.map((ghe) => {
                return ghe.tenGhe + ", ";
              })}
            </p>
          </div>
        </div>
      );
    });
  };

  return (
    <>
      <h2 className="text-4xl font-bold text-yellow-500 mb-5">
        Thông tin vé đã đặt
      </h2>
      <div className="grid md:grid-cols-2 grid-cols-1 gap-5">
        {handleRenderDanhSachPhimDaDat()}
      </div>
    </>
  );
}
