import { Tabs, Tag } from "antd";
import React, { useEffect, useLayoutEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { userService } from "../../services/userService";
import BookingInfo from "./BookingInfo/BookingInfo";
import UserInfo from "./UserInfo/UserInfo";

export default function UserPage() {
  let navigate = useNavigate();
  const [user, setUser] = useState({});

  useLayoutEffect(() => {
    userService
      .postThongTinTaiKhoan()
      .then((res) => {
        setUser(res.data.content);
        console.log(res.data.content.thongTinDatVe);
      })
      .catch((err) => {
        window.location.reload(false);
      });
  }, []);

  // Fixing Layout
  const glass_css = {
    background: "rgba( 0, 0, 0, 0.45 )",
    boxShadow: "0 8px 32px 0 rgba( 31, 38, 135, 0.37 )",
    backdropFilter: "blur( 2px )",
    borderRadius: "10px",
    border: "1px solid rgba( 255, 255, 255, 0.18 )",
  };

  //Tabs item
  const items = [
    {
      key: "1",
      label: <h2 className="">Thông tin tài khoản</h2>,
      className: "text-yellow-500 focus:font-bold target:text-red-500",
      children: (
        <div style={glass_css} className="p-5">
          <UserInfo user={user} />
        </div>
      ),
    },
    {
      key: "2",
      label: <h2 className="text-yellow-500">Thông tin vé đã đặt</h2>,
      children: (
        <div style={glass_css} className="text-white p-5">
          <BookingInfo thongTinDatVe={user?.thongTinDatVe} />
        </div>
      ),
    },
  ];

  return (
    <div className="container mx-auto grow py-10">
      <Tabs
        className="lg:hidden"
        tabBarStyle={{ color: "gold" }}
        tabPosition="top"
        defaultActiveKey="1"
        items={items}
      />
      <Tabs
        className="hidden lg:flex"
        tabBarStyle={{ color: "gold" }}
        tabPosition="left"
        defaultActiveKey="1"
        items={items}
      />
    </div>
  );
}
