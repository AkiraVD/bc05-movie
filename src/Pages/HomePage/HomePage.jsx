import React, { useEffect, useState } from "react";
import { movieService } from "../../services/movieService";
import BookingTab from "./BookingTab/BookingTab";
import MovieBanner from "./MovieBanner/MovieBanner";
import MovieList from "./MovieList/MovieList";
import MovieTabs from "./MovieTabs/MovieTabs";

export default function HomePage() {
  const [movieArr, setMovieArr] = useState([0]);
  const [bannerArr, setBannerArr] = useState([0]);
  useEffect(() => {
    movieService
      .getDanhSachPhim()
      .then((res) => {
        setMovieArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    movieService
      .getBanner()
      .then((res) => {
        setBannerArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <MovieBanner bannerArr={bannerArr} />
      <div className="container mx-auto">
        <br />
        <BookingTab movieArr={movieArr} />
        <br />
        <MovieList movieArr={movieArr} />
        <br />
        <MovieTabs />
      </div>
    </div>
  );
}
