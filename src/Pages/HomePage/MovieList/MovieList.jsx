import { nanoid } from "nanoid";
import React from "react";
import { NavLink } from "react-router-dom";

export default function MovieList({ movieArr }) {
  const renderMovieList = () => {
    return movieArr.slice(0, 12).map((item) => {
      return (
        <div
          key={nanoid()}
          className="w-full bg-white relative rounded overflow-hidden"
        >
          <img
            className="object-cover aspect-[2/3] w-full"
            src={item.hinhAnh}
            alt=""
          />
          <div className="absolute bottom-0 sm:top-0 left-0 h-full w-full text-center sm:p-5 sm:bg-black/[.7] sm:opacity-0 hover:opacity-100 duration-300">
            <h2 className="text-yellow-500 uppercase font-bold mb-5 bg-black/50 sm:bg-transparent mt-5 sm:mt-1">
              {item.tenPhim}
            </h2>
            <h3 className="text-white text-left break-words h-2/5 overflow-hidden hidden sm:block">
              {item.moTa?.length < 100
                ? item.moTa
                : item.moTa?.slice(0, 100) + "..."}
            </h3>
            <NavLink
              to={`/detail/${item.maPhim}`}
              className="bg-yellow-500 px-5 sm:py-2 sm:rounded border border-yellow-500 hover:bg-black sm:hover:bg-transparent hover:text-yellow-500 hover:border-yellow-500 duration-300 absolute bottom-0 left-0 right-0 sm:static"
            >
              Chi tiết
            </NavLink>
          </div>
        </div>
      );
    });
  };
  return (
    <div className="grid lg:grid-cols-4 sm:grid-cols-3 grid-cols-2 gap-5">
      {renderMovieList()}
    </div>
  );
}
