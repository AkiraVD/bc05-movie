import { useEffect, useState } from "react";
import { Select, Button, message } from "antd";
import { movieService } from "../../../services/movieService";
import moment from "moment";
import { useNavigate } from "react-router-dom";

export default function BookingTab({ movieArr }) {
  const navigate = useNavigate();
  const [state, setState] = useState(null);

  const [movie, setMovie] = useState();
  const [cinema, setCinema] = useState();
  const [bookingCode, setBookingCode] = useState();

  const [movieList, setMovieList] = useState([]);
  const [cinemaData, setCinemaData] = useState([]);
  const [cinemaList, setCinemaList] = useState([]);
  const [dateList, setDateList] = useState([]);

  useEffect(() => {}, []);

  useEffect(() => {
    // Tạo list phim mỗi khi nhận props movieArr
    renderDanhSachPhim();
  }, [movieArr]);

  useEffect(() => {
    // Tạo list rạp mỗi khi nhận chọn phim
    setCinema();
    setBookingCode();
    setState(movie);
    renderDanhSachCumRap();
  }, [movie]);

  useEffect(() => {
    // Tạo list lich chieu mỗi khi nhận chọn rạp
    setState(cinema);
    handleRenderDanhSachLichChieu(cinema);
  }, [cinema]);

  const renderDanhSachPhim = () => {
    let movies = movieArr.map((item) => {
      return { label: item.tenPhim, value: item.maPhim };
    });
    setMovieList(movies);
  };

  const renderDanhSachCumRap = () => {
    let cumRapData = [];
    movieService
      .getThongTinLichChieu(movie)
      .then((res) => {
        let dataRapChieu = res.data.content.heThongRapChieu;
        dataRapChieu.forEach((heThongRapChieu) => {
          cumRapData = [...cumRapData, ...heThongRapChieu.cumRapChieu];
        });
        setCinemaData(cumRapData);
        handleRenderListRapChieu(cumRapData);
      })
      .catch((err) => {
        if (movie == null) {
          return;
        }
        console.log(err);
      });
  };

  const handleRenderListRapChieu = (cumRapData) => {
    let cumRap = [];
    cumRap = cumRapData.map((item, cumRapIndex) => {
      return { label: item.tenCumRap, value: cumRapIndex };
    });
    setCinemaList(cumRap);
  };

  const handleRenderDanhSachLichChieu = (index) => {
    let dateData = cinemaData[index]?.lichChieuPhim?.map((item) => {
      return {
        label: moment(item.ngayChieuGioChieu).format("DD/MM/YYYY ~ hh:mm"),
        value: item.maLichChieu,
      };
    });
    setDateList(dateData);
  };

  const handleSummit = () => {
    if (movie == null) {
      return message.error("Bạn chưa chọn phim");
    } else if (cinema == null) {
      return message.error("Bạn chưa chọn rạp");
    } else if (bookingCode == undefined) {
      return message.error("Bạn chưa chọn ngày giờ chiếu");
    } else {
      navigate("/booking/" + bookingCode);
    }
  };

  const glass_css = {
    background: "rgba( 0, 0, 0, 0.45 )",
    boxShadow: "0 8px 32px 0 rgba( 31, 38, 135, 0.37 )",
    backdropFilter: "blur( 2px )",
    borderRadius: "10px",
    border: "1px solid rgba( 255, 255, 255, 0.18 )",
  };

  return (
    <div
      style={glass_css}
      className="py-4 sm:px-2 px-5 border border-gray-300 shadow-lg rounded "
    >
      <form className="flex sm:flex-row flex-col items-center sm:justify-evenly w-full gap-2 sm:gap-0">
        <Select
          value={movie}
          placeholder="Chọn phim"
          className="sm:w-1/4 sm:mr-4 w-full"
          options={movieList}
          onChange={setMovie} // Render danh sách rạp và set mã phim
        />
        <Select
          value={cinema}
          placeholder="Chọn rạp"
          className="sm:w-1/4 sm:mr-4 w-full"
          options={cinemaList}
          onChange={setCinema} // Render danh sách lịch chiếu và set rạp chiếu
        />
        <Select
          value={bookingCode}
          className="sm:w-1/4 sm:mr-4 w-full"
          placeholder="Chọn ngày giờ chiếu"
          options={dateList}
          onChange={setBookingCode} //Chọn mã chiếu phim và ngày giờ chiếu
        />
        <Button
          type="button"
          className="px-5 bg-yellow-500 hover:bg-transparent hover:text-yellow-500 hover:border-yellow-500"
          onClick={handleSummit}
        >
          Đặt vé
        </Button>
      </form>
    </div>
  );
}
