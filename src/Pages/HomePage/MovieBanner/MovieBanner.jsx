import React from "react";
import { Carousel } from "antd";
import { PlayCircleFilled } from "@ant-design/icons";
import { NavLink } from "react-router-dom";
import { nanoid } from "nanoid";

export default function MovieBanner({ bannerArr }) {
  const handleRenderBanner = () => {
    return bannerArr?.map((item) => {
      return (
        <div key={nanoid()} className="relative aspect-[16/9] ">
          <img
            src={item.hinhAnh}
            alt=""
            className="object-fill h-full w-full"
          />
          <div className="absolute top-0 left-0 h-full w-full flex justify-center items-center bg-black/25 sm:opacity-0 hover:opacity-100 duration-500">
            <NavLink to={`/detail/${item.maPhim}`}>
              <PlayCircleFilled className="text-6xl sm:text-9xl text-white/50 cursor-pointer hover:text-yellow-500 duration-300" />
            </NavLink>
          </div>
        </div>
      );
    });
  };

  return (
    <>
      <Carousel autoplay draggable>
        {handleRenderBanner()}
      </Carousel>
    </>
  );
}
