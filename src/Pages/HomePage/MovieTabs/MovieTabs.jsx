import React, { useEffect, useState } from "react";
import { movieService } from "../../../services/movieService";
import { Tabs } from "antd";
import MovieTabItem from "./MovieTabItem";
import { Desktop, Mobile, Tablet } from "../../../HOC/Responsive";
import { nanoid } from "nanoid";

const onChange = (key) => {
  console.log(key);
};

export default function MovieTabs() {
  const [dataMovie, setDataMovie] = useState([0]);
  useEffect(() => {
    movieService
      .getPhimTheoHeThongRap()
      .then((res) => {
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderHeThongRap = () => {
    return dataMovie.map((heThongRap) => {
      return {
        label: (
          <img
            className="w-16 h-16 object-cover"
            src={heThongRap.logo}
            alt=""
          />
        ),
        key: nanoid(),
        children: (
          <Tabs
            tabBarStyle={{
              maxHeight: "500px",
              overflow: "hidden",
              width: "400px",
            }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            // items=""
            items={heThongRap.lstCumRap?.map((cumRap) => {
              return {
                label: (
                  <div className="text-left w-[22rem]">
                    <p className="text-green-500 font-semibold uppercase">
                      {cumRap.tenCumRap}
                    </p>
                    <p className="text-white active:text-black-300 truncate">
                      {cumRap.diaChi}
                    </p>
                    <p className="text-red-500 font-semibold">[chi tiết]</p>
                  </div>
                ),
                key: nanoid(),
                children: (
                  <div style={{ maxHeight: "550px", overflow: "auto" }}>
                    {cumRap.danhSachPhim?.map((phim) => {
                      return <MovieTabItem key={nanoid()} movie={phim} />;
                    })}
                  </div>
                ),
              };
            })}
          />
        ),
      };
    });
  };

  let renderHeThongRapMobile = () => {
    return dataMovie.map((heThongRap) => {
      return {
        label: (
          <img
            className="w-10 h-10 object-cover"
            src={heThongRap.logo}
            alt=""
          />
        ),
        key: nanoid(),
        children: (
          <div className="h-[400px] overflow-auto">
            {heThongRap.lstCumRap?.map((item) => {
              return (
                <p
                  key={nanoid()}
                  className="text-green-500 font-semibold uppercase break-words mb-3 w-[275px]"
                >
                  {item.tenCumRap}
                </p>
              );
            })}
          </div>
        ),
      };
    });
  };

  return (
    <div className="bg-black/50 py-5 w-max mx-auto mb-10 rounded scrollbar-hide">
      <h2 className="text-yellow-500 text-center mb-5 text-xl md:text-4xl font-bold">
        DANH SÁCH RẠP
      </h2>
      <Desktop>
        <div>
          <Tabs
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderHeThongRap()}
          />
        </div>
      </Desktop>
      <Tablet>
        <div>
          <Tabs
            tabPosition="top"
            centered="true"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderHeThongRap()}
          />
        </div>
      </Tablet>
      <Mobile>
        <Tabs
          tabPosition="left"
          centered="true"
          defaultActiveKey="1"
          onChange={onChange}
          items={renderHeThongRapMobile()}
        />
      </Mobile>
    </div>
  );
}
