import moment from "moment";
import { nanoid } from "nanoid";
import React from "react";
import { NavLink, useNavigate } from "react-router-dom";

export default function MovieTabItem({ movie }) {
  const navigate = useNavigate();

  let content = (
    <div className="flex p-3 text-left w-max">
      <img
        src={movie.hinhAnh}
        alt=""
        className="w-24 h-32 object-cover mr-5 rounded"
      />
      <div className="w-auto">
        <NavLink
          className="text-yellow-500 font-semibold"
          to={`/detail/${movie.maPhim}`}
        >
          <span className="bg-orange-500 px-1 text-white rounded">C18</span>{" "}
          {movie.tenPhim}
        </NavLink>
        <div className="grid grid-cols-1 lg:grid-cols-2 justify-items-start gap-4 mt-3 ">
          {movie.lstLichChieuTheoPhim?.slice(0, 4).map((item) => {
            return (
              <span
                key={nanoid()}
                className="bg-gray-500/25 border-inherit p-2 rounded font-semibold cursor-pointer"
                onClick={() => {
                  navigate("/booking/" + item.maLichChieu);
                }}
              >
                <span className="text-green-500">
                  {moment(item.ngayChieuGioChieu).format("DD/MM/YYYY ")}
                </span>
                ~
                <span className="text-red-500">
                  {moment(item.ngayChieuGioChieu).format(" hh:mm")}
                </span>
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
  return <>{content}</>;
}
