import React from "react";
import Lottie from "lottie-react";
import not_found_animate from "../../assets/not-found.json";
import { NavLink } from "react-router-dom";
import { Button } from "antd";

export default function NotFoundPage() {
  return (
    <div className="flex flex-col justify-center items-center h-screen w-screen">
      <h4 className="text-6xl text-red-600 font-medium animate-bounce mb-10">
        404 PAGE NOT FOUND
      </h4>
      <Lottie animationData={not_found_animate} loop={true} />
      <NavLink to="/">
        <Button
          type="button"
          className="mt-10 px-5 bg-yellow-500 hover:bg-white hover:text-yellow-500 hover:border-yellow-500"
        >
          Quay về trang chủ &#x2192;
        </Button>
      </NavLink>
    </div>
  );
}
