import { message } from "antd";
import { SET_USER_INFO } from "../constant/userConstant";
import { userLocalService } from "../../services/localStorageService";
import { userService } from "../../services/userService";

export const setLoginAction = (value) => {
  return {
    type: SET_USER_INFO,
    payload: value,
  };
};

export const setLoginActionService = (userForm, onSuccess) => {
  return (dispatch) => {
    userService
      .postDangNhap(userForm)
      .then((res) => {
        console.log(res);
        message.success("Đăng nhập thành công");
        // Luu local storage
        userLocalService.set(res.data.content);
        dispatch({
          type: SET_USER_INFO,
          payload: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        console.log(err);
        message.error("Đã có lỗi xảy ra");
      });
  };
};
