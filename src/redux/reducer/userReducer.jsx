import { SET_USER_INFO } from "../constant/userConstant";
import { userLocalService } from "../../services/localStorageService";

const initialState = {
  userInfo: userLocalService.get(),
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER_INFO:
      return { ...state, userInfo: payload };

    default:
      return state;
  }
};
