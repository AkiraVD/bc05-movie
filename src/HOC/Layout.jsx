import React from "react";
import Footer from "../Components/Footer/Footer";
import Header from "../Components/Header/Header";
import background_img from "../assets/movie-bg.jpg";

export default function Layout({ children }) {
  const backgroundStyle = {
    backgroundImage: `url(${background_img})`,
    minHeight: "100vh",
    backgroundAttachment: "fixed",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
  };
  return (
    <div style={backgroundStyle} className="flex flex-col justify-between">
      <Header />
      {children}
      <Footer />
    </div>
  );
}
