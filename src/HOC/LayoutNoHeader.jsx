import React from "react";
import background_img from "../assets/movie-bg.jpg";

export default function LayoutNoHeader({ children }) {
  const backgroundStyle = {
    backgroundImage: `url(${background_img})`,
    minHeight: "100vh",
    backgroundAttachment: "fixed",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
  };
  return (
    <div style={backgroundStyle} className="">
      {children}
    </div>
  );
}
