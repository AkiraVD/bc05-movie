import { useSelector } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Spinner from "./Components/Spinner/Spinner";
import Layout from "./HOC/Layout";
import LayoutNoHeader from "./HOC/LayoutNoHeader";
import BookingPage from "./Pages/BookingPage/BookingPage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";
import SignUpPage from "./Pages/SignUpPage/SignUpPage";
import AdminPage from "./Pages/Admin/AdminPage/AdminPage";
import UserPage from "./Pages/UserPage/UserPage";

function App() {
  return (
    <div>
      <Spinner />
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />

          <Route
            path="/login"
            element={
              <LayoutNoHeader>
                <LoginPage />
              </LayoutNoHeader>
            }
          />
          <Route
            path="/signup"
            element={
              <LayoutNoHeader>
                <SignUpPage />
              </LayoutNoHeader>
            }
          />
          <Route
            path="/user"
            element={
              <Layout>
                <UserPage />
              </Layout>
            }
          />
          {/* admin module page */}

          <Route path="/admin" element={<AdminPage />} />

          <Route path="*" element={<NotFoundPage />} />
          <Route
            path="/detail/:id"
            element={
              <Layout>
                <DetailPage />
              </Layout>
            }
          />
          <Route
            path="/booking/:id"
            element={
              <Layout>
                <BookingPage />
              </Layout>
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
