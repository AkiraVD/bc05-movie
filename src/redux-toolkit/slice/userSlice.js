import { createSlice } from "@reduxjs/toolkit";
import { userLocalService } from "../../services/localStorageService";

const initialState = {
  userInfo: userLocalService.get(),
};

export const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserInfo: (state, action) => {
      state.userInfo = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setUserInfo } = userSlice.actions;

export default userSlice.reducer;
